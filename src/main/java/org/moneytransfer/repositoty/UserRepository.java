package org.moneytransfer.repositoty;

import org.moneytransfer.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
