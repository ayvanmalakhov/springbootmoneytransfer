package org.moneytransfer.repositoty;

import org.moneytransfer.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
