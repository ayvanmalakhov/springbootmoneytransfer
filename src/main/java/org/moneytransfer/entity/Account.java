package org.moneytransfer.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.UUID;


import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column
  private UUID uuid;
  @Column
  private String number;
  @Column
  private BigDecimal balance;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {
          CascadeType.MERGE,
          CascadeType.REFRESH
  })
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "user_id")
  private User user;

  private Currency currency;


  public Account(UUID uuid, String number, BigDecimal balance) {
    this.uuid = uuid;
    this.number = number;
    this.balance = balance;
  }
}
