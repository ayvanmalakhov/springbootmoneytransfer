package org.moneytransfer.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  private String firstName;

  @Column
  private String secondName;

  @OneToMany(mappedBy = "user", cascade = {
          CascadeType.MERGE,
          CascadeType.REFRESH
  }, fetch = FetchType.LAZY)
  @Fetch(FetchMode.JOIN)
  private Collection<Account> accounts;

/*
  public User(String firstName, String secondName) {
    this.firstName = firstName;
    this.secondName = secondName;
  }
*/
}
