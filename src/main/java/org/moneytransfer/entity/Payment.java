package org.moneytransfer.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Data
public class Payment {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @OneToOne
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "accountFrom_id", referencedColumnName = "id")
  private Account accountFrom;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @Fetch(FetchMode.JOIN)
  @JoinColumn(name = "accountTo_id", referencedColumnName = "id")
  private Account accountTo;

  @Column
  private Date dt;
  @Column
  private BigDecimal amount;
}
