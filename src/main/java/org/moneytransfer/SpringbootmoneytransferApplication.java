package org.moneytransfer;

import lombok.extern.slf4j.Slf4j;
import org.moneytransfer.entity.Account;
import org.moneytransfer.entity.Customer;
import org.moneytransfer.entity.User;
import org.moneytransfer.repositoty.AccountRepository;
import org.moneytransfer.repositoty.CustomerRepository;
import org.moneytransfer.repositoty.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.UUID;

@SpringBootApplication
@Slf4j
public class SpringbootmoneytransferApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootmoneytransferApplication.class, args);
  }

  @Bean
  public CommandLineRunner TestAccountRepository (AccountRepository accountRepository,
                                                  UserRepository userRepository){
    return (args)->{
      User newUser = new User();
      newUser.setFirstName("Ivan");
      newUser.setSecondName("Malakhov");
      userRepository.save(newUser);

      Account account = new Account(UUID.randomUUID(),
                                    "12345678901234567890",
                                    BigDecimal.ZERO);
      account.setUser(newUser);
      accountRepository.save(account);
/*      accountRepository.save(new Account(UUID.randomUUID(),
                                         "09876543210987654321",
                                         BigDecimal.ZERO,
                                         newUser));
*/

      // fetch all customers
      log.info("User found with findAll():");
      log.info("-------------------------------");
      for (User users : userRepository.findAll()) {
        log.info(users.toString());
      }
      log.info("");

      // fetch all customers
      log.info("Account found with findAll():");
      log.info("-------------------------------");
      for (Account accounts : accountRepository.findAll()) {
        log.info(accounts.toString());
      }
      log.info("");
    };

  }
/*
  @Bean
  public CommandLineRunner demo(CustomerRepository repository) {
    return (args) -> {
      // save a few customers
      repository.save(new Customer("Jack", "Bauer"));
      repository.save(new Customer("Chloe", "O'Brian"));
      repository.save(new Customer("Kim", "Bauer"));
      repository.save(new Customer("David", "Palmer"));
      repository.save(new Customer("Michelle", "Dessler"));

      // fetch all customers
      log.info("Customer found with findAll():");
      log.info("-------------------------------");
      for (Customer customer : repository.findAll()) {
        log.info(customer.toString());
      }
      log.info("");

      // fetch an individual customer by ID
      Customer customer = repository.findById(1L);
      log.info("Customer found with findById(1L):");
      log.info("--------------------------------");
      log.info(customer.toString());
      log.info("");

      // fetch customers by last name
      log.info("Customer found with findByLastName('Bauer'):");
      log.info("--------------------------------------------");
      repository.findByLastName("Bauer").forEach(bauer -> {
        log.info(bauer.toString());
      });
      // for (Customer bauer : repository.findByLastName("Bauer")) {
      //  log.info(bauer.toString());
      // }
      log.info("");
    };
  }
*/
}
